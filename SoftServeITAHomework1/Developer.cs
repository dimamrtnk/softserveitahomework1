﻿namespace SoftServeITAHomework1
{
    public class Developer : Employee
    {
        /// <summary>
        /// Base constructor
        /// </summary>
        public Developer() : base()
        {   
        }
        /// <summary>
        /// User constructor
        /// </summary>
        /// <param name="manager"></param>
        public Developer(Manager manager) : base(manager)
        {
        }
        /// <summary>
        /// User constructor
        /// </summary>
        /// <param name="manager">Manager of this Developer</param>
        /// <param name="firstName">Developer's firstname </param>
        /// <param name="secondName">Developer's secondname</param>
        public Developer(Manager manager, string firstName, string secondName) : base(manager, firstName, secondName)
        {
        }
        /// <summary>
        /// User constructor
        /// </summary>
        /// <param name="manager">Manager of this Developer</param>
        /// <param name="firstName">Developer's firstname </param>
        /// <param name="secondName">Developer's secondname</param>
        /// <param name="salary">Developer's salary</param>
        public Developer(Manager manager, string firstName, string secondName, double salary) : base(manager, firstName, secondName, salary)
        {
        }
    }
}
