﻿using System.Collections.Generic;

namespace SoftServeITAHomework1
{
    public class Department
    {
        /// <summary>
        /// List of managers
        /// </summary>
        public List<Manager> Managers { get; set; }
        public Department()
        {
            Managers = new List<Manager>();
        }
        public Department(List<Manager> managers)
        {
            Managers = managers;
        }
        /// <summary>
        /// Print salary info of each employee in the department
        /// </summary>
        public void PrintEmployeesSalary()
        {
            foreach (var manager in Managers)
            {
                manager.PrintSalary();
                foreach (var teamMember in manager.DreamTeam)
                {
                    teamMember.PrintSalary();
                }
            }
        }
    }
}
