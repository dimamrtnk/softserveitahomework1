﻿using System.Collections.Generic;
using System.Linq;

namespace SoftServeITAHomework1
{
    public class Manager : Employee
    {
        /// <summary>
        /// Manager's team of developers and designers
        /// </summary>
        public List<Employee> DreamTeam { get; set; }
        /// <summary>
        /// Base constructor
        /// </summary>
        public Manager()
        {
            DreamTeam = new List<Employee>();
        }
        /// <summary>
        /// User constructor
        /// </summary>
        /// <param name="team">Manager's team of developers and designers</param>
        public Manager(List<Employee> team)
        {
            DreamTeam = team;
        }
        // Calculate the total salary with all bonuses, etc
        protected override double CalculateSalary(double salary)
        {
            double salaryCoef = 1;//salary multiplier 
            int bonus = 0;//additional bonus based on team size
            if (DreamTeam.Where(x => x is Developer).Count() > DreamTeam.Count / 2)//if more than half of the team members are developers
            {
                salaryCoef = 1.1;//multiply salary by 1.1
            }
            if (DreamTeam.Count > 10)
                bonus = 300;
            else if (DreamTeam.Count > 5)
                bonus = 200;
            return base.CalculateSalary(salary * salaryCoef) + bonus;
        }
    }
}
