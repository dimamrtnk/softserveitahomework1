﻿using System;

namespace SoftServeITAHomework1
{
    class Program
    {
        static void Main(string[] args)
        {
            Department department = new Department();
            Manager manager1 = new Manager()
            {
                FirstName = "Dmitry",
                SecondName = "Martynenko",
                Salary = 6000,
                Experience = 10
            };
            Manager manager2 = new Manager()
            {
                FirstName = "Alexey",
                SecondName = "Privalov",
                Salary = 4000,
                Experience = 7
            };
            

            manager1.DreamTeam.Add(new Developer(manager1, "Ruslan", "Antonuk", 3000) { Experience = 3.5 });
            manager1.DreamTeam.Add(new Developer(manager1, "Vladislav", "Chernenko", 60) { Experience = 0 });
            manager1.DreamTeam.Add(new Developer(manager1, "Vladislav", "Tkachenko", 2000) { Experience = 2 });
            manager1.DreamTeam.Add(new Developer(manager1, "Ruslan", "Nalgiev", 700) { Experience = 0.5 });
            manager1.DreamTeam.Add(new Designer(manager1, "Sofiya", "Dolgopolova", 2000, 0.8) { Experience = 2 });
            manager1.DreamTeam.Add(new Designer(manager1, "Yana", "Shupik", 2700, 1) { Experience = 2.5 });

            manager2.DreamTeam.Add(new Developer(manager2, "Maxim", "Bilogur", 3000) { Experience = 1 });
            manager2.DreamTeam.Add(new Developer(manager2, "Bogdan", "Sosluk", 250) { Experience = 0 });
            manager2.DreamTeam.Add(new Designer(manager2, "Vladimir", "Burdov", 800, 0.3) { Experience = 1.5 });
            manager2.DreamTeam.Add(new Designer(manager2, "Denis", "Gromadsky", 1700, 0.7) { Experience = 0.5 });

            department.Managers.Add(manager1);
            department.Managers.Add(manager2);

            Console.WriteLine("-----------------------------------List of employees-----------------------------------");
            foreach (var man in department.Managers)
            {
                Console.WriteLine(man);
                foreach (var tm in man.DreamTeam)
                {
                    Console.WriteLine(tm);
                }
            }
            Console.WriteLine("---------------------------------------------------------------------------------------");
            Console.WriteLine("-----------------------------------Emloyees salary-------------------------------------");
            department.PrintEmployeesSalary();
            Console.WriteLine("---------------------------------------------------------------------------------------");


            Console.ReadKey();
        }
    }
}
