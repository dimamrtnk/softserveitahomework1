﻿using System;

namespace SoftServeITAHomework1
{
    public class Designer : Employee
    {
        private double effCoeff;
        /// <summary>
        /// Effectivness coefficient
        /// </summary>
        public double EffCoeff
        {
            get => effCoeff;
            set
            {
                if (value < 0 || value > 1)
                    throw new ArgumentException("Effectiveness coefficient should be in range of 0 and 1");
                else
                    effCoeff = value;
            }
        }
        /// <summary>
        /// Base constructor
        /// </summary>
        public Designer() : base()
        {
            EffCoeff = 0;
        }
        /// <summary>
        /// User contructor
        /// </summary>
        /// <param name="manager">Manager of this designer</param>
        /// <param name="effCoeff">Effectivness coefficient. Should be in range of 0.0 and 1.0</param>
        public Designer(Manager manager, double effCoeff) : base(manager)
        {
            EffCoeff = effCoeff;
        }
        /// <summary>
        /// User contructor
        /// </summary>
        /// <param name="manager">Manager of this designer</param>
        /// <param name="firstName">Designer's firstname </param>
        /// <param name="secondName">Designer's secondname</param>
        /// <param name="effCoeff">Effectivness coefficient. Should be in range of 0.0 and 1.0</param>
        public Designer(Manager manager, string firstName, string secondName, double effCoeff) : base(manager, firstName, secondName)
        {
            EffCoeff = effCoeff;
        }
        /// <summary>
        /// User contructor
        /// </summary>
        /// <param name="manager">Manager of this designer</param>
        /// <param name="firstName">Designer's firstname </param>
        /// <param name="secondName">Designer's secondname</param>
        /// <param name="salary">Designer's salary in USD</param>
        /// <param name="effCoeff">Effectivness coefficient. Should be in range of 0.0 and 1.0</param>
        public Designer(Manager manager, string firstName, string secondName, double salary, double effCoeff) 
            : base(manager, firstName, secondName, salary)
        {
            EffCoeff = effCoeff;
        }
        // Calculate the total salary with all bonuses, etc
        protected override double CalculateSalary(double salary)
        {
            return base.CalculateSalary(salary * effCoeff);
        }
    }
}
