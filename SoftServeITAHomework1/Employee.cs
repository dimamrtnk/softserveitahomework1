﻿using System;

namespace SoftServeITAHomework1
{
    public abstract class Employee
    {
        private const int MONTHS_COUNT = 12;
        private double experience, salary;
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public Manager Manager { get; set; }
        public double Salary
        {
            get => CalculateSalary(salary);
            set
            {
                if (value < 0)//validate salary value
                    throw new ArgumentException("Salary should not be negative!", "Salary");
                else salary = value;
            }
        }
        public double Experience
        {
            get => experience;
            set
            {
                if (value < 0)//validate experience value
                    throw new ArgumentException("Experience can't be negative!", "Experience");
                else experience = value;

            }
        }

        public Employee()
        {
            Manager = new Manager();
        }

        public Employee(Manager manager)
        {
            Manager = manager;
        }
        public Employee(Manager manager, string firstName, string secondName)
        {
            Manager = manager;
            FirstName = firstName;
            SecondName = secondName;
        }

        public Employee(Manager manager, string firstName, string secondName, double salary)
        {
            Manager = manager;
            FirstName = firstName;
            SecondName = secondName;
            Salary = salary;
        }
        /// <summary>
        /// String representation for employee
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (this is Manager)
                return $"{FirstName} {SecondName}, experience: {GetExperienceString()}";
            else return $"{FirstName} {SecondName}, manager: {Manager?.SecondName}, experience: {GetExperienceString()}";
        }
        /// <summary>
        /// Print employee full name and his salary
        /// </summary>
        public void PrintSalary()
        {
            Console.WriteLine($"{FirstName} {SecondName}: got salary: {Salary}");
        }

        // Calculate the total salary with all bonuses, etc
        protected virtual double CalculateSalary(double salary)
        {
            if (experience > 5)//if employee's experience is more than 5 years
                return salary * 1.2 + 500;//more money!
            else if (experience > 2)//if more than 2 years
                return salary + 200;//also more money, but a little bit less
            else return salary;
        }
        //string representation for employee's experience
        private string GetExperienceString()
        {
            int whole = (int)Math.Truncate(experience);//the whole part of experience number
            double fractional = experience - whole;//fractional part of experience number
            if (experience == 0)//if no experience
                return "Employee has no experience yet :(";
           
            if (whole == 0)//if exp is less than 1 year
            {
                return $"{MONTHS_COUNT * fractional} months"; //return how many moths of experience
            }
            else
            {
                if (fractional == 0)//if exp is an integer number 
                    return $"{whole} years";//return how many years of experience
                else return $"{whole} years {(int)(MONTHS_COUNT * fractional)} months"; //else return how many years and moths of experience
            }
        }
        
       
    }
    
}
